from pyrevit import revit
from pyrevit import forms
from pyrevit import DB

for selected_element in revit.get_selection():
    old_family = selected_element.Symbol.Family
    old_name = old_family.Name
    if old_family:
        new_name = forms.ask_for_string(default='Duplicate ' + old_name, prompt="Enter family name:", title='Duplicate Family')

        if new_name == old_name:
            continue

        with revit.Transaction("Temp rename"):
            old_family.Name = new_name

        temp_doc = revit.doc.EditFamily(old_family)

        with revit.Transaction("Rename back"):
            old_family.Name = old_name

        temp_doc.LoadFamily(revit.doc)
        temp_doc.Close(False)