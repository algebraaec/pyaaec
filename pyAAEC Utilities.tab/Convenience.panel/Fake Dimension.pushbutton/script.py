from pyrevit import revit
from pyrevit import forms
from pyrevit import DB

override_string = forms.ask_for_string(default='Override', prompt="Enter new text to replace dimension:", title='Dimensional Override')
selected = revit.pick_elements_by_category(DB.BuiltInCategory.OST_Dimensions, "Select dimensions to override")

with revit.Transaction("Override dimensions"):
    for dim in selected:
        dim.ValueOverride = override_string
