# -*- coding: utf-8 -*-
# pylint: skip-file

__title__ = 'Copy Imported Categories'
__doc__ = """Transfers the visibility/graphics override settings for imported categories from one view template to another.
"""

from pyrevit import script, revit, DB
from pyrevit.forms import SelectFromList
from pyrevit.revit import doc, uidoc

output = script.get_output()
logger = script.get_logger()
linkify = output.linkify

ALLOWED_TYPES = ( # These are all subclasses of DB.View
    DB.ViewPlan,
    DB.View3D,
    DB.ViewSection,
    DB.ViewSheet,
    DB.ViewDrafting
)

def get_view_templates(doc, view_type=None):
    allviews = \
        DB.FilteredElementCollector(doc).WhereElementIsNotElementType()\
                                     .OfCategory(DB.BuiltInCategory.OST_Views)\
                                     .ToElements()

    return filter(lambda v: v.IsTemplate and (view_type is None or v.ViewType == view_type), allviews)


def get_imported_categories(doc):
    imported_instances = DB.FilteredElementCollector(doc).OfClass(DB.ImportInstance).ToElements()
    cat_ids = { imp.Category.Id for imp in imported_instances } # Unique categories
    cats = [ DB.Category.GetCategory(doc, id) for id in cat_ids]

    return sorted(cats, key=lambda c:c.Name)


def get_active_view():
    active_view = doc.ActiveView
    if not isinstance(active_view, ALLOWED_TYPES):
        logger.error('Selected view is not allowed. Please select or open view from which '
                        'you want to copy template settings VG Overrides - Filters')
        return
    return active_view


def main():
    active_view = get_active_view()
    if not active_view:
        logger.warning('Activate a view to copy')
        return

    logger.debug('Source view selected: %s id%s' % (active_view.Name, active_view.Id.ToString()))

    # Imported Category selection =======================================================
    active_template_imported_parent_cats = get_imported_categories(doc)

    viewtemplate_list = get_view_templates(doc)

    if not viewtemplate_list:
        logger.warning('Project has no view templates')
        return

    if not active_template_imported_parent_cats:
        logger.warning('Active view has no imported categories overrides')
        return

    cat_checkboxes = \
        SelectFromList.show(
            active_template_imported_parent_cats,
            name_attr='Name',
            title='Select imported categories to copy',
            button_name='Select imported categories',
            multiselect=True
        ) or []

    cat_checkboxes_sel = list(cat_checkboxes)

    # Select subcategories from parent categorogies in active view
    for parent_cat in cat_checkboxes:
        cat_checkboxes_sel.extend(parent_cat.SubCategories)

    if len(cat_checkboxes_sel) == 0:
        return

    # Target view templates selection =========================================
    view_checkboxes_sel = SelectFromList.show(viewtemplate_list, name_attr='Name', title='Select templates to apply imported categories',
                                          button_name='Apply imported categories to templates', multiselect=True)

    if len(view_checkboxes_sel) == 0:
        return

    with revit.Transaction(__title__):
        for vt in view_checkboxes_sel:
            if vt.Id == active_view.ViewTemplateId:
                logger.debug('Current template found')
                continue

            for cat_element in cat_checkboxes_sel:
                overrides = active_view.GetCategoryOverrides(cat_element.Id)
                hidden = active_view.GetCategoryHidden(cat_element.Id)
                try:
                    vt.SetCategoryOverrides(cat_element.Id, overrides) # set the overrides no matter the visibility
                    vt.SetCategoryHidden(cat_element.Id, hidden)
                except Exception as e:
                    logger.error('Could not copy category %s to view %s\n%s' % (cat_element.Id.IntegerValue.ToString(),
                                                                                vt.Name, e))


if __name__ == "__main__":
    main()