# Electrical Connector
from pyrevit import revit, forms, DB, UI
from pyrevit.revit import doc

forms.check_familydoc(exitscript=True)

"""
If there's exactly one existing electrical connector, then we want to
re-use it. However, if there are multiple, then we'll assume the user
knows what's up and wants to keep them as is.
"""
is_power = lambda con: con.SystemClassification == DB.MEPSystemClassification.PowerBalanced or con.SystemClassification == DB.MEPSystemClassification.PowerUnBalanced

all_connectors = DB.FilteredElementCollector(doc).OfCategory(DB.BuiltInCategory.OST_ConnectorElem)
electrical_connectors = [ con for con in all_connectors if is_power(con)]

t = DB.Transaction(doc)
t.Start("Add an electrical connector")

if len(electrical_connectors) == 1:
    connector = electrical_connectors[0]
    connector.SystemClassification = DB.MEPSystemClassification.PowerBalanced
else:
    # user selects the face
    ref = revit.uidoc.Selection.PickObject(UI.Selection.ObjectType.Face)
    # creates the connector on the face selected by the user
    connector = DB.ConnectorElement.CreateElectricalConnector(revit.doc, DB.Electrical.ElectricalSystemType.PowerBalanced, ref) # ConnectorElement class

# Get the "AAEC "-prefixed family parameters. Create a dict with the key being the name stripped of leading "AAEC "
parameters = {str(p.Definition.Name).split("AAEC ")[-1]: p for p in doc.FamilyManager.GetParameters() if "AAEC " in p.Definition.Name}

# get the connector properties
properties = connector.GetOrderedParameters() # Parameter class

# Associate family parameters with properties
# We cycle through all the connector properties and check to see if there's a corresponding entry in the parameters dict
for prop in properties:
    prop_name = prop.Definition.Name

    # If we're a lighting fixture family then "Apparent Load" gets mapped to "AAEC Total Electrical Load". Otherwise it's just "AAEC Electrical Load"
    if prop_name == "Apparent Load":
        prop_name = "Total Electrical Load" if DB.BuiltInCategory.OST_LightingFixtures == doc.OwnerFamily.FamilyCategory.BuiltInCategory else "Electrical Load"

    if prop_name in parameters:
        param = parameters[prop_name]
        if param.StorageType == prop.StorageType:
            doc.FamilyManager.AssociateElementParameterToFamilyParameter(prop, param)

t.Commit()