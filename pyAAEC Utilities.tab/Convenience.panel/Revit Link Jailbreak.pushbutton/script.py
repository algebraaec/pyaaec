from collections import namedtuple
from os import path

import tempfile
import shutil

from pyrevit import revit, DB, HOST_APP, forms, framework, script

"""
Process an instance or RevitLinkType to extract its short name, Model ID, and
Project ID. The property named `.description` is what gets read by the info
panel in the form dialog.

"""
def get_reference_info(rvt_link):
    resource_reference = list(dict(rvt_link.GetExternalResourceReferencesExpanded()).values())[0][0]
    name = resource_reference.GetResourceShortDisplayName()

    reference_info = resource_reference.GetReferenceInformation()
    model_id = reference_info["LinkedModelModelId"]
    project_id = reference_info["LinkedModelProjectId"]
    description = "{}:\n{}".format(model_id, resource_reference.InSessionPath)

    RevitLink = namedtuple("RevitLink", ["name", "model_id", "project_id", "description"])

    return RevitLink(name, model_id, project_id, description)


link_elements = DB.FilteredElementCollector(revit.doc)\
                    .OfClass(framework.get_type(DB.RevitLinkType))\
                    .ToElements()

links = sorted([get_reference_info(e) for e in link_elements], key=lambda l:l.name)
result = forms.SelectFromList.show(links, name_attr="name", title="Select Revit Link to Jailbreak", button_name="Select Link", info_panel=True, width=1400)

if result is None:
    script.exit()

"""
Rather than try to expand `%LOCALAPPDATA%` from within python, we grab the path
from the active journal filename. Not sure if this works for all forms of
roaming profiles or if it's better. On my setup, journal_dir will expand to:

`%LOCALAPPDATA%\Autodesk\Revit\Autodesk Revit 20xx\Journals`

The linked model is then a path relative to that directory.
"""
journal_dir = path.dirname(HOST_APP.app.RecordingJournalFilename)
relative = r"..\CollaborationCache\{user_id}\{result.project_id}\LinkedModels\{result.model_id}.rvt".format(user_id=HOST_APP.app.LoginUserId, result=result)
cached_filename = path.join(journal_dir, relative)

"""
For some reason, we need to copy the file before opening it detached. To be
proper, we would clean up after ourselves and delete the temp folder, but (at
least on the VM) the folder already gets deleted when the user signs out of
Windows, so no sweat.
"""
temp_dir = tempfile.mkdtemp()
shutil.copy(cached_filename, temp_dir)
temp_fname = path.join(temp_dir, path.basename(cached_filename))
model_path = DB.FilePath(temp_fname)

open_options = DB.OpenOptions(DetachFromCentralOption=DB.DetachFromCentralOption.DetachAndPreserveWorksets)
new_doc = HOST_APP.uiapp.OpenAndActivateDocument(model_path, open_options, True)
