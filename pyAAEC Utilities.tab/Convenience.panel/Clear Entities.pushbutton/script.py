from pyrevit import revit, DB, script
from pyrevit.revit import doc

def clear_storage(el):
    for guid in el.GetEntitySchemaGuids():
        el.DeleteEntity(DB.ExtensibleStorage.Schema.Lookup(guid))

result = revit.pick_elements_by_category(DB.BuiltInCategory.OST_DetailComponents, message="Please select detail items from which to clear the associated extensible storage")

if result is None:
    script.exit()

with revit.Transaction("Clear Entities"):
    for el in result:
        clear_storage(el)